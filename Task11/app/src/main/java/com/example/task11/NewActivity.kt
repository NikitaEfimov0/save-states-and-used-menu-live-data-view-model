package com.example.task11

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider

class NewActivity : AppCompatActivity() {

     lateinit var viewModel:ViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new)
        val button = findViewById<Button>(R.id.button)
        val tEdit = findViewById<EditText>(R.id.textEdit)
        val tView = findViewById<TextView>(R.id.textView)
        viewModel = ViewModelProvider(this).get(ViewModel::class.java)

        viewModel.current.observe(this, Observer {
            tView.text = it.toString()
        })

        button.setOnClickListener {
            val string = tEdit.text.toString()
            viewModel.string = string
            viewModel.current.value = viewModel.string
        }

    }



}