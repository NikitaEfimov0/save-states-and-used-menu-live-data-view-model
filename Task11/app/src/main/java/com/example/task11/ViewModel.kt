package com.example.task11
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
class ViewModel:ViewModel() {
    var string = ""

    val current: MutableLiveData<String> by lazy{
        MutableLiveData<String>()
    }
}