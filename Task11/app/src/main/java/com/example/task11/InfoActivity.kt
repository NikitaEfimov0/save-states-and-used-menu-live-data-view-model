package com.example.task11

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView

class InfoActivity : AppCompatActivity() {
    var greetings:TextView? = null
    var info:TextView? = null
    var image:ImageView? = null
    var typeOfDragon:String? = null
    var infoAboutDragon:String? = null
    var back:LinearLayout? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)
        val button:Button = findViewById(R.id.back)
        back = findViewById(R.id.backgr)
        image = findViewById(R.id.portrait)
        greetings = findViewById(R.id.gc)
        info = findViewById(R.id.allInfo)
        supportActionBar?.hide()
        whichDragon();
        writeGc();
        writeInfo();
        button.setOnClickListener {
            val intent: Intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }

    fun whichDragon(){
        if(Person.age%2==0 && ((Person.age/2)%2==0 || Person.age/2==1)){
            image?.setImageResource(R.drawable.nightdragon)
            back?.setBackgroundResource(R.drawable.nightdragon)
            typeOfDragon = "НОЧНОЙ ДРАКОН"
            infoAboutDragon = NightDragon.info
        }
        else if(Person.age%13==0){
            image?.setImageResource(R.drawable.deathdragon)
            back?.setBackgroundResource(R.drawable.deathdragon)
            typeOfDragon = "ДРАКОН СМЕРТИ"
            infoAboutDragon = DeathDragon.info
        }
        else if(Person.age%23 == 0){
            image?.setImageResource(R.drawable.undergrounddragon)
            back?.setBackgroundResource(R.drawable.undergrounddragon)
            typeOfDragon = "ДРАКОН ПОДЗЕМЬЯ"
            infoAboutDragon = UndergroundDragon.info
        }
        else if(Person.age%7 == 0){
            image?.setImageResource(R.drawable.voiddragon)
            back?.setBackgroundResource(R.drawable.voiddragon)
            typeOfDragon = "ДРАКОН ПУСТОТЫ"
            infoAboutDragon = VoidDragon.info
        }
        else if(Person.age%3 == 0){
            image?.setImageResource(R.drawable.stormdragon)
            back?.setBackgroundResource(R.drawable.stormdragon)
            typeOfDragon = "ШТОРМОВОЙ ДРАКОН"
            infoAboutDragon = StormDragon.info
        }
        else{
            image?.setImageResource(R.drawable.polardragon)
            back?.setBackgroundResource(R.drawable.polardragon)
            typeOfDragon = "ДРАКОН СЕВЕРА"
            infoAboutDragon = PolarDragon.info
        }
    }

    fun writeGc(){
        val str:String = "Вы - $typeOfDragon"
        greetings?.setText(str)
    }

    fun writeInfo(){
        val str:String = "${Person.surname} ${Person.name} ${Person.patronymic}, Вам выпала огромная честь узнать, кровь какого дракона течет в Ваших жилах.\n" +
                "$infoAboutDragon\n" +
                "Ваши увлечения (${Person.hobby}) соотвествуют Вашим драконьим корням.\n" +
                "Надеюсь, Вас не шокировала та информация, которую Вы выяснили о себе. Попутного ветра Вам в крылья и вкусной добычи!"
        info?.setText(str)
    }
}