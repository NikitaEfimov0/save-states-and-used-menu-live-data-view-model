package com.example.task11
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.Surface
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlin.random.Random


class MainActivity : AppCompatActivity() {
    private var n:EditText? = null
    private var s:EditText? = null
    private var p:EditText? = null
    private var a:EditText? = null
    private var h:EditText? = null
    private var layout:LinearLayout? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        layout = findViewById(R.id.mainLayout)
        val button:Button = findViewById(R.id.button)
        val exit:Button = findViewById(R.id.exit)
        n = findViewById(R.id.name)
        s = findViewById(R.id.surname)
        p = findViewById(R.id.patronymic)
        a = findViewById(R.id.age)
        h = findViewById(R.id.hobby)
        loadDate()

        button.setOnClickListener(this::sendInfo)
        exit.setOnClickListener{
            val sharedPr = getSharedPreferences("sharedPr", Context.MODE_PRIVATE)
            sharedPr.edit().clear().apply()
            finishAffinity()
        }
    }

    fun sendInfo(view: View){
        if(a?.text?.isEmpty() == true) {
            Toast.makeText(this, "Вы заполнили не все поля!", Toast.LENGTH_SHORT).show()
            return
        }

        Person.name = n?.text.toString()
        Person.surname = s?.text.toString()
        Person.patronymic = p?.text.toString()
        Person.age = a?.text.toString().toInt()
        Person.hobby = h?.text.toString()
        if(Person.name == "" || Person.surname == "" || Person.patronymic == "" || Person.age == 0 || Person.hobby == ""){
            Toast.makeText(this, "Вы заполнили не все поля!", Toast.LENGTH_SHORT).show()
            return
        }
        val intent = Intent(this, InfoActivity::class.java)
        startActivity(intent)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.changing->{
                var x = retInt()
                when(x){
                    1->layout?.setBackgroundResource(R.drawable.dragon)
                    2->layout?.setBackgroundResource(R.drawable.dragon2)
                    3->layout?.setBackgroundResource(R.drawable.dragon3)
                    4->layout?.setBackgroundResource(R.drawable.drgaon4)
                    5->layout?.setBackgroundResource(R.drawable.dragon5)
                    6->layout?.setBackgroundResource(R.drawable.dragon6)
                    7->layout?.setBackgroundResource(R.drawable.dragon7)
                    8->layout?.setBackgroundResource(R.drawable.dragon8)
                    9->layout?.setBackgroundResource(R.drawable.dragon9)
                }
            }
            R.id.save->{
                saveData()
            }

            R.id.creation->{
                saveData()
                startActivity(Intent(this, NewActivity::class.java))
            }
        }
        return true
    }
    private fun saveData(){
        var name = n?.text.toString()
        var surname = s?.text.toString()
        var patronymic = p?.text.toString()
        var age = a?.text.toString()
        var hobby = h?.text.toString()

        val sharedPr = getSharedPreferences("sharedPr", Context.MODE_PRIVATE)
        val editor = sharedPr.edit()
        editor.apply{
            putString("NAME", name)
            putString("SURNAME", surname)
            putString("PATRONYMIC", patronymic)
            putString("AGE", age)
            putString("HOBBY", hobby)
        }.apply()

    }

    fun retInt():Int{
        val random:Int = Random.nextInt(1, 9)
        return random
    }

    private fun loadDate(){
        val sharedPr = getSharedPreferences("sharedPr", Context.MODE_PRIVATE)
        n?.setText(sharedPr.getString("NAME", null))
        s?.setText(sharedPr.getString("SURNAME", null))
        p?.setText(sharedPr.getString("PATRONYMIC", null))
        a?.setText(sharedPr.getString("AGE", null))
        h?.setText(sharedPr.getString("AGE", null))
    }

}